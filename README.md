# Инструкция по запуску и настройке

## Requirements:
1. [mongodb](https://docs.mongodb.com/manual/administration/install-on-linux/) 
2. [node](https://nodejs.org/en/download/) -- npm входит в архив.
3. [Webstorm](https://www.jetbrains.com/ru-ru/webstorm/)

## Настройка mongo
После установки запустите демон mongod(сервер, который будет работать в фоновом режиме).
Укажите путь, где будет храниться ваша db.

Пример

`sudo mongod --pathdb database/db`

Для администрирования вашей бд в терминале в новом окне наберите

`sudo mongo`

## Подключение модулей

Начнем с бекенда. Откройте webstorm и правый щелчок на `package.json`. И установите модули.

Тоже самое для клиента.

Следующий шаг настройка путей и портов.

## process.env
Измените Express sekret key на придуманный свой(это для сокетов).
Имя бд можете взять свое, так как используется принцип CodeToFirst.

```
EXPRESS_PORT=4000
EXPRESS_SECRET=NmZ[.v7!h,87xkpgxB>Rnx'M@)}C1
MONGODB_HOST=127.0.0.1
MONGODB_PORT=27017
DB_NAME=quizzer
MAX_QUESTIONS_PER_ROUND=12
```
В файле `index.js` измените путь к файлу `process.env`

`require("dotenv").config({ path: "/home/bokoch/quizzer/server/process.env" })
`
## cmd
Это отдельный проект по загрузке вопросов в вашу бд через json.
В `index.js` пропишите путь к файлу   `process.env`. Поместите файл в папку, чтобы импортировать записи.

### Запуск
Если ничего не забыл, то пора запустить проект. 
Запустите сначала сервер с помощью ide или командой в папке server/src.

`node index.js`

Как только в терминале будет прописано, что сервер подключен, запустите клиент.

`npm start`